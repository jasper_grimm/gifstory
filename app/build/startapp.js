var ReactCSSTransitionGroup = React.addons.CSSTransitionGroup;

var VideoItem = React.createClass({displayName: "VideoItem",
	
	getInitialState: function() {
		return {
			duration: 3,
            video: this.props.video
		}
	},
    //
    componentDidMount: function() {

        var video = document.getElementsByTagName('video')[0]

        video.addEventListener('loadeddata', function() {
            video.className = "video-loaded"
            video.play()
        })

        video.onended = function(e) {
            this.props.onEnd(e)
        }.bind(this);


    },
    //
    //componentWillUpdate: function() {
    //    console.log('componentWillUpdate')
    //},

    componentWillReceiveProps: function(props) {
        this.setState({
            video: props.video
        })
    },

    getPoster: function() {
        if(!this.state.video) {
            return ''
        }

        var hash = /^http:\/\/media.giphy.com\/media\/(\w+)\/giphy/.exec(this.state.video)[1]
        return "";//"https://media.giphy.com/media/" + hash + "/200_s.gif"
    },

    _render: function() {
        return (
            React.createElement("video", {autoBuffer: true, autoPlay: true, width: "100%", height: "100%", poster: this.getPoster()}, 
                React.createElement("source", {src: this.state.video}), 
                "Video not supported"
            )
        )
    },

    render: function() {
        return React.cloneElement(this._render())
    }

});


var VideoItems = React.createClass({displayName: "VideoItems",

    data: [

        {video: "http://media.giphy.com/media/hdn9WPuQSemC4/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/tMt7jsVXyECaY/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/BW614moAKgxUc/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/ztmGb71eK8LDO/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/4l6OYDqVrJJSg/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/A3uHl6VZ0mCTC/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/hIBoVkbj14lm8/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/YWLgBX7PM3oR2/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/AH83XjQWH1yHC/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/7ePXPCHol6K1G/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/AJkdZG9SatmTu/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/13lP4u4X9aNMv6/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/qb3mVKL74GgF2/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/SGNjkI6MbJxYI/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/kqCc0j23thfws/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/iZGUZ5HkGx2HS/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/IopZ7CXe1FIHu/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/edfYHdnNTaKgE/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/12xldTDRaDRo9G/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/SmnJwLKQ8FM6k/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/8LOm41q1egGti/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/lto2wrNGI3I8U/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/y6eKtscgsbklO/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/WETFvYyrU61LG/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/3j0GNxlkIXFtK/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/aLUfX9Tj2XZRe/giphy.mp4"}, //

        {video: "http://media.giphy.com/media/lN2usWgo3oLbG/giphy.mp4"}, // http://giphy.com/gifs/love-crazy-bebeandbebe-iI9ln7jfK8V4A
        {video: "http://media.giphy.com/media/aQScZW7mRqwx2/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/fRnpBMqzJbYqI/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/W4j7BzL6Fz6Ja/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/AKhw9CmgRJd9S/giphy.mp4"}, //
        {video: "http://media.giphy.com/media/RbQzUFNHF8316/giphy.mp4"}, //
    ],

    getInitialState: function() {
        return {
            current_video_index: 0,
            item: this.data[0]
        }
    },

    start: function() {
        this.setState({
            current_video_index: 0,
            item: this.data[0]
        }, function(){
            var video = document.getElementsByTagName('video')[0]
            video.load()
        })
    },

    handleEnd: function() {
        this.start()
    },

    handleEndItem: function(e) {
        var video = document.getElementsByTagName('video')[0]
        video.className = "video"
        setTimeout(function() {
            if (this.state.current_video_index + 1 < this.data.length) {
                this.setState({
                    current_video_index: this.state.current_video_index + 1,
                    item: this.data[this.state.current_video_index + 1]
                }, function() {
                    video.load()

                }.bind(this))
            } else {
                this.handleEnd()
            }
        }.bind(this), 700);


    },

	render: function() {

		return (
			React.createElement("div", {style: {width: "100%", height: "100%", display: 'table', backgroundColor: "black"}}, 
                React.createElement(VideoItem, {video: this.state.item.video, onEnd: this.handleEndItem}), 
                React.createElement("audio", {autoPlay: true}, 
                    React.createElement("source", {src: "ee8a0b778c35bf.mp3", type: "audio/mpeg"})
                )
			)
		)

	}
})


var Footer = React.createClass({displayName: "Footer",
	render: function() {
		return (
			React.createElement("div", {className: "container"}, 
				React.createElement("div", {className: "row"}, 
					React.createElement("div", {className: "col-lg-10 col-lg-offset-1 text-center"}, 
						React.createElement("h4", null, 
							React.createElement("strong", null, "Lovely Gif Story")
						), 
						React.createElement("ul", {className: "list-unstyled"}, 
							React.createElement("li", null, React.createElement("i", {className: "fa fa-envelope-o fa-fw"}), "  ", React.createElement("a", {href: "mailto:jasper@grimmlab.com"}, "jasper@grimmlab.com"))
						), 
						React.createElement("br", null), 
						React.createElement("hr", {className: "small"}), 
						React.createElement("p", {className: "text-muted"}, "Copyright © GrimmLab 2015")
					)
				)
			)
		)
	}
})

React.render(
	(
		React.createElement("div", {style: {width: "100%", height: "100%"}}, 
			React.createElement(VideoItems, null), 
			React.createElement("footer", null, 
				React.createElement(Footer, null)
			)
		)
	),
	document.getElementById("app")
)